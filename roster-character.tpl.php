<?php

/**
 * @file character.tpl.php
 * Default theme implementation to display a character.
 */
?>
<div id="roster-character">
	<h3><?php print t('Profile'); ?></h3>
	<table>
		<tr>
			<td class="portrait" rowspan="5"><img class="portrait" src="<?php print $image; ?>" alt="" /></td>
			<td class="field"><?php print t('Name:'); ?></td>
			<td class="value"><?php print $first_name; ?></td>
			<td class="field"><?php print t('Profession:'); ?></td>
			<td class="value"><?php print $profession; ?></td>
		</tr>
		<tr>
			<td class="field"><?php print t('Surname:'); ?></td>
			<td class="value"><?php print $last_name; ?></td>
			<td class="field"><?php print t('Faction:'); ?></td>
			<td class="value"><?php print $faction; ?></td>
		</tr>
		<tr>
			<td class="field"><?php print t('Level:'); ?></td>
			<td class="value"><?php print $level; ?></td>
			<td class="field"><?php print t('Rank:'); ?></td>
			<td class="value"><?php print $rank; ?></td>
		</tr>
		<tr>
			<td class="field"><?php print t('Race:'); ?></td>
			<td class="value"><?php print $race; ?></td>
			<td class="field"><?php print t('Pilot faction:'); ?></td>
			<td class="value"><?php print $pilot_faction; ?></td>
		</tr>
		<tr>
			<td class="field"><?php print t('Gender:'); ?></td>
			<td class="value"><?php print $gender; ?></td>
			<td class="field"><?php print t('Pilot level:'); ?></td>
			<td class="value"><?php print $pilot_level; ?></td>
		</tr>
	</table>
	
<?php if (isset($biography)): ?>
	<div id="biography">
		<h3><?php print t('Biography'); ?></h3>
		<p class="biography"><?php print $biography; ?></p>
		<?php if (isset($more_info)): ?><p><?php print l(t('More information'), $more_info); ?></p><?php endif; ?>
	</div>
<?php endif; ?>

<?php if (isset($other_chars)): ?>
	<div id="other-chars">
		<h3><?php print t('Other characters from this user'); ?></h3>
		<ul>
		<?php foreach ($other_chars as $nid => $title): ?>
			<li><?php print l($title, "node/$nid"); ?></li>
		<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>

</div>
