<?php

/**
 * @file roster-block.tpl.php
 * Default theme implementation to display a block.
 */
?>
<ul class="menu">
<?php
	foreach ($roster as $nid => $char) {
		$name = $char->first_name;
		if (!empty($char->last_name)) {
			$name .= ' ' . $char->last_name;
		}
		print '<li class="leaf">' . l($name, "node/$nid") . '</li>';
	}
?>
</ul>
