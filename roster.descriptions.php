<?php


/**
 * Return a list of professions
 */
function roster_get_professions() {
	static $professions;
	if (empty($professions)) {
		$professions = array(
			1 => 'Bounty Hunter',
			2 => 'Commando',
			3 => 'Entertainer',
			4 => 'Jedi: Dark',
			5 => 'Jedi: Light',
			6 => 'Medic',
			7 => 'Officer',
			8 => 'Smuggler',
			9 => 'Spy',
			10 => 'Trader: Domestic Goods',
			11 => 'Trader: Engineering',
			12 => 'Trader: Munitions',
			13 => 'Trader: Structures',
		);
	}
	return $professions;
}

/**
 * Return a profession name given its ID
 */
function roster_get_profession_by_id($id = 0) {
	static $professions;
	if (empty($professions)) {
		$professions = roster_get_professions();
	}
	return $professions[$id];
}

/**
 * Return a list of ranks
 */
function roster_get_ranks() {
	static $ranks;
	if (empty($ranks)) {
		$ranks = array(
			1 => 'Private',
			2 => 'Lance Corporal',
			3 => 'Corporal',
			4 => 'Sergeant',
			5 => 'Master Sergeant',
			6 => 'Sergeant Major',
			7 => 'Lieutenant',
			8 => 'Captain',
			9 => 'Major',
			10 => 'Lieutenant Colonel',
			11 => 'Colonel',
			12 => 'General',
		);
	}
	return $ranks;
}

/**
 * Return a rank name given its ID
 */
function roster_get_rank_by_id($id = 0) {
	static $ranks;
	if (empty($ranks)) {
		$ranks = roster_get_ranks();
	}
	return $ranks[$id];
}

/**
 * Return a list of factions
 */
function roster_get_factions() {
	static $factions;
	if (empty($factions)) {
		$factions = array(
			1 => 'Imperial',
			2 => 'Neutral',
			3 => 'Rebel',
		);
	}
	return $factions;
}

/**
 * Return a faction name given its ID
 */
function roster_get_faction_by_id($id = 0) {
	static $factions;
	if (empty($factions)) {
		$factions = roster_get_factions();
	}
	return $factions[$id];
}

/**
 * Return a list of races
 */
function roster_get_races() {
	static $races;
	if (empty($races)) {
		$races = array(
			1 => 'Bothan',
			2 => 'Human',
			3 => 'Ithorian',
			4 => 'Mon Calamari',
			5 => 'Rodian',
			6 => 'Sullustan',
			7 => 'Trandoshan',
			8 => 'Twi\'lek',
			9 => 'Wookiee',
			10 => 'Zabrak',
		);
	}
	return $races;
}

/**
 * Return a race name given its ID
 */
function roster_get_race_by_id($id = 0) {
	static $races;
	if (empty($races)) {
		$races = roster_get_races();
	}
	return $races[$id];
}

/**
 * Return a list of genders
 */
function roster_get_genders() {
	static $genders;
	if (empty($genders)) {
		$genders = array(
			1 => 'Male',
			2 => 'Female',
		);
	}
	return $genders;
}

/**
 * Return a gender name given its ID
 */
function roster_get_gender_by_id($id = 0) {
	static $genders;
	if (empty($genders)) {
		$genders = roster_get_genders();
	}
	return $genders[$id];
}

/**
 * Return a list of pilot tiers
 */
function roster_get_tiers() {
	static $tiers;
	if (empty($tiers)) {
		$tiers = array(
			0 => 'Tier 0',
			1 => 'Tier 1',
			2 => 'Tier 2',
			3 => 'Tier 3',
			4 => 'Tier 4',
			5 => 'Tier 5: Master Pilot',
		);
	}
	return $tiers;
}

/**
 * Return a pilot tier name given its ID
 */
function roster_get_tier_by_id($id = 0) {
	static $tiers;
	if (empty($tiers)) {
		$tiers = roster_get_tiers();
	}
	return $tiers[$id];
}

/**
 * Return the image associated to a race
 */
function roster_get_image_by_race_id($id = 0, $gender = 1) {
	static $images;
	if (empty($images)) {
		$images = array(
			1 => 'bothan',
			2 => 'human',
			3 => 'ithorian',
			4 => 'mon_calamari',
			5 => 'rodian',
			6 => 'sullustan',
			7 => 'trandoshan',
			8 => 'twilek',
			9 => 'wookiee',
			10 => 'zabrak',
		);
	}
	static $path;
	if (empty($path)) {
		$path = base_path() . drupal_get_path('module', 'roster') . '/img/';
	}
	return ($path . $images[$id] . '_' . strtolower(roster_get_gender_by_id($gender)) . '.jpg');
}
