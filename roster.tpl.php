<?php

/**
 * @file roster.tpl.php
 * Default theme implementation to display a roster.
 */
?>
<table id="roster">
	<thead>
		<tr>
			<th><?php print t('Name'); ?></th>
			<th><?php print t('Surname');?></th>
			<th><?php print t('Level'); ?></th>
			<th><?php print t('Race'); ?></th>
			<th><?php print t('Gender'); ?></th>
			<th><?php print t('Profession'); ?></th>
			<th><?php print t('Rank'); ?></th>
			<th><?php print t('Faction'); ?></th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($roster as $nid => $char): ?>
		<tr>
			<td class="first-name"><a href="<?php print $char->link; ?>"><?php print $char->first_name; ?></a></td>
			<td class="last-name"><?php print $char->last_name; ?></td>
			<td class="level"><?php print $char->level; ?></td>
			<td class="race"><?php print $char->race; ?></td>
			<td class="gender"><?php print $char->gender; ?></td>
			<td class="profession"><?php print $char->profession; ?></td>
			<td class="rank"><?php print $char->rank; ?></td>
			<td class="faction"><?php print $char->faction; ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<?php print $pager; ?>
