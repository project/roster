<?php

/**
 * Implementation of hook_install().
 */
function roster_install() {
	// Create tables.
	drupal_install_schema('roster');

	// Notify of changes
	drupal_set_message(t('Roster module was successfully installed with default options.'));
	drupal_set_message(t('Module options are available at the <a href="!settings">content management settings page</a>.', array('!settings' => url('admin/content/roster'))));
	drupal_set_message(t('You may want to <a href="!navigation">add a new navigation menu entry</a> for users to access the roster.', array('!navigation' => url('admin/build/menu-customize/navigation'))));
	drupal_set_message(t('You may also want to <a href="!permissions">set the permissions</a> for users to use the new module.', array('!permissions' => url('admin/user/permissions'))));
	drupal_set_message('A new content type "character" was created.');
}

/**
 * Implementation of hook_uninstall().
 */
function roster_uninstall() {
	// Drop tables and delete variables
	drupal_uninstall_schema('roster');
	variable_del('roster_chars_per_page');
}

/**
 * Implementation of hook_schema().
 */
function roster_schema() {
	$schema['roster'] = array(
		'description' => t('Stores the relationship of nodes to characters.'),
		'fields' => array(
			'nid' => array(
				'description' => t('The {node}.nid of the character.'),
				'type' => 'int',
				'size' => 'normal',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'vid' => array(
				'description' => t('The {node}.vid of the character.'),
				'type' => 'int',
				'size' => 'normal',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'first_name' => array(
				'description' => t('Name of the character.'),
				'type' => 'varchar',
				'size' => 'normal',
				'length' => 255,
				'not null' => TRUE,
				'default' => '',
			),
			'last_name' => array(
				'description' => t('Surname of the character.'),
				'type' => 'varchar',
				'size' => 'normal',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'gender' => array(
				'description' => t('Gender of the character.'),
				'type' => 'int',
				'size' => 'normal',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'level' => array(
				'description' => t('The (combat) level of the character.'),
				'type' => 'int',
				'size' => 'normal',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 1,
			),
			'profession' => array(
				'description' => t('The profession of the character.'),
				'type' => 'int',
				'size' => 'normal',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'rank' => array(
				'description' => t('The (military) rank of the character.'),
				'type' => 'int',
				'size' => 'normal',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 1,
			),
			'faction' => array(
				'description' => t('The faction of the character.'),
				'type' => 'int',
				'size' => 'normal',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'race' => array(
				'description' => t('The race of the character.'),
				'type' => 'int',
				'size' => 'normal',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'pilot_faction' => array(
				'description' => t('The faction of the character in space.'),
				'type' => 'int',
				'size' => 'normal',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'pilot_level' => array(
				'description' => t('The level of the character in space.'),
				'type' => 'int',
				'size' => 'normal',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'more_info' => array(
				'description' => t('A link to another source of information of the character.'),
				'type' => 'varchar',
				'size' => 'normal',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'biography' => array(
				'description' => t('The background of the character.'),
				'type' => 'text',
				'size' => 'normal',
				'not null' => FALSE,
				'default' => '',
			),
		),
		'indexes' => array(
			'nid' => array('nid'),
			'profession' => array('profession'),
			'rank' => array('rank'),
			'faction' => array('faction'),
			'gender' => array('gender'),
			'pilot_faction' => array('pilot_faction'),
			'pilot_level' => array('pilot_level'),
 		),
		'unique keys' => array(
		),
		'primary key' => array('vid'),
	);

	return $schema;
}
