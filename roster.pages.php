<?php

/**
 * Returns a list of characters
 */
function roster_get_characters($chars_per_page) {
	$roster = array();

	$stmt = "SELECT r.*
			   FROM {node} n
		 INNER JOIN {roster} r
				 ON n.vid = r.vid
			  WHERE n.status = 1
		   ORDER BY r.first_name, r.last_name, r.nid";
	$query = db_rewrite_sql($stmt);
	$result = pager_query($query, $chars_per_page, 0, NULL);
	
	while ($char = db_fetch_object($result)) {
		$roster[$char->nid] = $char;
	}
	return $roster;
}

/**
 * Displays the content of the page
 */
function roster_page() {
	$chars_per_page = variable_get('roster_chars_per_page', 25);

	$roster = roster_get_characters($chars_per_page);

	// return theme('roster', $roster, $chars_per_page);
	return theme('roster', $roster);
}
